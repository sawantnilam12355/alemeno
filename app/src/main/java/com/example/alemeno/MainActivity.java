package com.example.alemeno;

import androidx.appcompat.app.AppCompatActivity;
import pl.droidsonroids.gif.GifImageView;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alemeno.tools.CommonMethodes;
import com.example.alemeno.tools.SessionManager;

public class MainActivity extends AppCompatActivity {
    View feed_apple;
    TextView tv_stageNo,tv_fruitNo,tv_feed_fruit;
    ImageView img_ani;
    int stage, fruit_eaten;
    SessionManager manager;
    Handler gifhandler;
    GifImageView gifviewid;
    private final int GIF_DISPLAY_LENGTH = 2000;
    MediaPlayer sound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CommonMethodes.hideNavigation(this);
        setContentView(R.layout.activity_main);
        manager =new SessionManager(this);
        initializeview();
    }

    private void initializeview() {
        feed_apple =findViewById(R.id.feed_apple);
        tv_stageNo =findViewById(R.id.tv_stageNo);
        tv_fruitNo =findViewById(R.id.tv_fruitNo);
        tv_feed_fruit =findViewById(R.id.tv_feed_fruit);
        img_ani =findViewById(R.id.img_ani);
        gifviewid = findViewById(R.id.gifviewid);

        tv_stageNo.setText(manager.getStage());
        tv_fruitNo.setText(manager.getFruitEaten());
        tv_feed_fruit.setText(manager.getfeedtext());

        setAnimalImage();

        feed_apple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Refreshdata();
            }
        });
    }

    private void setAnimalImage() {
        int stage_no = Integer.parseInt(manager.getStage());

        if (stage_no==1){
            img_ani.setImageResource(R.drawable.ic_ani1);
        }else if (stage_no ==2){
            img_ani.setImageResource(R.drawable.ic_ani2);
        }else if (stage_no ==3){
            img_ani.setImageResource(R.drawable.ic_ani3);
        }else if (stage_no ==4){
            img_ani.setImageResource(R.drawable.ic_ani4);
        }else if (stage_no ==5){
            img_ani.setImageResource(R.drawable.ic_ani5);
        }
    }

    private void Refreshdata() {
        stage = Integer.parseInt(manager.getStage());
        fruit_eaten = Integer.parseInt(manager.getFruitEaten());

        if (fruit_eaten ==0){
            tv_fruitNo.setText("1");
            manager.setEatenFruit("1");
            setgif();
        }else if (fruit_eaten ==1){
            tv_fruitNo.setText("2");
            manager.setEatenFruit("2");
            setgif();
        }else if (fruit_eaten ==2){
            tv_fruitNo.setText("3");
            manager.setEatenFruit("3");
            setgif();
        }else if (fruit_eaten ==3){
            tv_fruitNo.setText("4");
            manager.setEatenFruit("4");
            setgif();
        }else if (fruit_eaten ==4){
            tv_fruitNo.setText("5");
            manager.setEatenFruit("5");
            manager.setStage("2");
            tv_stageNo.setText(manager.getStage());
            img_ani.setImageResource(R.drawable.ic_ani2);
            setgif();
        }else if (fruit_eaten ==5){
            tv_fruitNo.setText("6");
            manager.setEatenFruit("6");
            setgif();
        }else if (fruit_eaten ==6){
            tv_fruitNo.setText("7");
            manager.setEatenFruit("7");
            setgif();
        }else if (fruit_eaten ==7){
            tv_fruitNo.setText("8");
            manager.setEatenFruit("8");
            setgif();
        }else if (fruit_eaten ==8){
            tv_fruitNo.setText("9");
            manager.setEatenFruit("9");
            setgif();
        }else if (fruit_eaten ==9){
            tv_fruitNo.setText("10");
            manager.setEatenFruit("10");
            manager.setStage("3");
            tv_stageNo.setText(manager.getStage());
            img_ani.setImageResource(R.drawable.ic_ani3);
            setgif();
        }else if (fruit_eaten ==10){
            tv_fruitNo.setText("11");
            manager.setEatenFruit("11");
            setgif();
        }else if (fruit_eaten ==11){
            tv_fruitNo.setText("12");
            manager.setEatenFruit("12");
            setgif();
        }else if (fruit_eaten ==12){
            tv_fruitNo.setText("13");
            manager.setEatenFruit("13");
            setgif();
        }else if (fruit_eaten ==13){
            tv_fruitNo.setText("14");
            manager.setEatenFruit("14");
            setgif();
        }else if (fruit_eaten ==14){
            tv_fruitNo.setText("15");
            manager.setEatenFruit("15");
            manager.setStage("4");
            tv_stageNo.setText(manager.getStage());
            img_ani.setImageResource(R.drawable.ic_ani4);
            setgif();
        }else if (fruit_eaten ==15){
            tv_fruitNo.setText("16");
            manager.setEatenFruit("16");
            setgif();
        }else if (fruit_eaten ==16){
            tv_fruitNo.setText("17");
            manager.setEatenFruit("17");
            setgif();
        }else if (fruit_eaten ==17){
            tv_fruitNo.setText("18");
            manager.setEatenFruit("18");
            setgif();
        }else if (fruit_eaten ==18){
            tv_fruitNo.setText("19");
            manager.setEatenFruit("19");
            setgif();
        }else if (fruit_eaten ==19){
            tv_fruitNo.setText("20");
            manager.setEatenFruit("20");
            manager.setStage("5");
            tv_stageNo.setText(manager.getStage());
            manager.setfeedtext("Start Again");
            tv_feed_fruit.setText(manager.getfeedtext());
            img_ani.setImageResource(R.drawable.ic_ani5);
            setgif();
        }else if (fruit_eaten ==20){
           manager.setStage("1");
           manager.setEatenFruit("0");
           tv_stageNo.setText(manager.getStage());
           tv_fruitNo.setText(manager.getFruitEaten());
           manager.setfeedtext("Feed Apple");
           tv_feed_fruit.setText(manager.getfeedtext());
            img_ani.setImageResource(R.drawable.ic_ani1);
        }
    }

    private void setgif() {
        gifviewid.setVisibility(View.VISIBLE);
        img_ani.setVisibility(View.GONE);
        sound =MediaPlayer.create(this,R.raw.eating_sound);
        sound.start();
        gifHandler();
    }

    private void gifHandler(){
        gifhandler =new Handler();
        gifhandler.postDelayed(gif_time,GIF_DISPLAY_LENGTH);
    }

    final Runnable gif_time = new Runnable() {
        @Override
        public void run() {
            img_ani.setVisibility(View.VISIBLE);
            gifviewid.setVisibility(View.GONE);
            sound.stop();
        }
    };
}
