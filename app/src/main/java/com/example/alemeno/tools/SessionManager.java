package com.example.alemeno.tools;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionManager<T> {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;
    SharedPreferences.Editor editor1;
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;
    private static SessionManager instance;

    public static final String STAGE = "stage";
    public static final String FRUIT_EATEN = "fruit_eaten";
    public static final String FEED_TEXT = "feed_text";

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(context.getPackageName(), PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setStage(String stage) {

        // Storing name in pref
        editor.putString(STAGE, stage);
        // commit changes
        editor.commit();
    }

    public String getStage() {
        String selectedLang = pref.getString(STAGE,"1");
        return selectedLang;
    }

    public void setEatenFruit(String eatenFruit) {

        // Storing name in pref
        editor.putString(FRUIT_EATEN, eatenFruit);
        // commit changes
        editor.commit();
    }

    public String getFruitEaten() {
        String selectedLang = pref.getString(FRUIT_EATEN,"0");
        return selectedLang;
    }

    public void setfeedtext(String feedtext) {

        // Storing name in pref
        editor.putString(FEED_TEXT, feedtext);
        // commit changes
        editor.commit();
    }

    public String getfeedtext() {
        String selectedLang = pref.getString(FEED_TEXT,"Feed Apple");
        return selectedLang;
    }


}
